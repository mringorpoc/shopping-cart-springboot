# shopping-cart-spring-microservice
Shopping cart created using reactjs client, spring boot, eureka, feign and mysql.

## Description

This project is just a demo. Different components can be used in production. e.g. kubernetes cloud over eureka. ibm api gateway instead of spring-boot api gateway.
Shopping-cart client is built using reactjs which connects to spring boot api-gateway. In this project, api-gateway protects internal microservices with an api key with internal circuit breaker using resilience4j. 
Internal microservices are made of springboot which talks to each other using feign (can be improved to use kafka) and registered to eureka (service discovery).

The product-service loads products from a remote api https://fakestoreapi.com/products and loads it into is local database.
The basket-service loads products from product-service via feign to check for stock inventory and price. It allows creation of basket for a customer also the items the customer wants. 
The customer-service allows crud of customer and loads baskets from basket-service via feign and allows basket to relate to customer.

![Scheme](ShoppingCart.png)

## Building each service


### Build discovery-service
```
cd discovery-service
mvn clean package spring-boot:repackage
```

### Build api-gateway
Layer on top of internal microservices.
```
cd api-gateway
mvn clean package spring-boot:repackage
```

### Build product-service
```
cd product-service
mvn clean package spring-boot:repackage
```
### Build basket-service

```
cd basket-service
mvn clean package spring-boot:repackage
```

### Build customer-service
```
cd customer-service
mvn clean package spring-boot:repackage
```

### Creating an .env file
```
MYSQL_ROOT_PASSWORD=myapp
MYSQL_DATABASE=myapp
MYSQL_USER=myapp
MYSQL_PASSWORD=myapp
SECURITY_USER=admin
SECURITY_PASSWORD=admin
```

### Dockerize them all
```
cd root
docker-compose up -d
```

### Check discovered services
```
https://eureka.mringork8s.one
```

### Health check api
```
https://shopping-cart-api.mringork8s.one/actuator/health
https://shopping-cart-api.mringork8s.one/actuator/customer/health
https://shopping-cart-api.mringork8s.one/actuator/product/health
https://shopping-cart-api.mringork8s.one/actuator/basket/health
```

### Check open api specification
```
https://shopping-cart-api.mringork8s.one/customer/swagger-ui.html
https://shopping-cart-api.mringork8s.one/basket/swagger-ui.html
https://shopping-cart-api.mringork8s.one/product/swagger-ui.html
```

### Shopping cart
```
https://shopping-cart.mringork8s.one
```
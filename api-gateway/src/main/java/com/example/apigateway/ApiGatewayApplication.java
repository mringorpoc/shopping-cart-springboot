package com.example.apigateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

import com.example.apigateway.security.APIKeyFilter;

@EnableEurekaClient
@SpringBootApplication
public class ApiGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiGatewayApplication.class, args);
	}
	
    @Bean
    public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
	        .route("PRODUCT-SERVICE", r -> r.path("/api/products/**").filters(f -> f.filter(new APIKeyFilter())).uri("lb://PRODUCT-SERVICE"))
	        .route("PRODUCT-SERVICE", r -> r.path("/product/**").uri("lb://PRODUCT-SERVICE"))
	        .route("BASKET-SERVICE", r -> r.path("/api/baskets/**").uri("lb://BASKET-SERVICE"))
	        .route("BASKET-SERVICE", r -> r.path("/basket/**").uri("lb://BASKET-SERVICE"))
	        .route("CUSTOMER-SERVICE", r -> r.path("/api/customers/**").uri("lb://CUSTOMER-SERVICE"))
	        .route("CUSTOMER-SERVICE", r -> r.path("/customer/**").uri("lb://CUSTOMER-SERVICE"))
	        .build();
    }

}

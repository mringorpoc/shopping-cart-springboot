package com.example.basketservice.controller;

import java.util.Collections;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.basketservice.model.Basket;
import com.example.basketservice.model.Item;
import com.example.basketservice.service.BasketService;
import com.example.basketservice.service.ItemService;
import com.example.basketservice.exception.ResourceNotFoundException;

@RestController
@RequestMapping("/api")
public class BasketController {
    
	BasketService basketservice;
	ItemService itemService;
	
    @Autowired
    public BasketController(BasketService basketservice, ItemService itemService) {
        this.basketservice = basketservice;
        this.itemService = itemService;
    }
    
    @GetMapping(value="/baskets")
    public Iterable<BasketResponse> getAllBaskets(){    	
        return Streamable.of(basketservice.getAllBaskets()).map(basket -> {
        	Iterable<Item> items = itemService.findByBasketId(basket.getId());//getAllBasketsByCustomerId(customer.getId().intValue());
            return new BasketResponse(basket, items);
    	}).toList();
    }  
    
    @GetMapping(value="/baskets/{id}")
    public BasketResponse getBasketById(@PathVariable("id") @Min(1) int id) {
        Basket basket = basketservice.findById(id)
                                    .orElseThrow(()->new ResourceNotFoundException("Basket with "+id+" is Not Found!"));
        
        Iterable<Item> items = Optional.of(itemService.findByBasketId(id)).orElseGet(Collections::emptyList); 
        return new BasketResponse(basket, items);
    }  
    
    @GetMapping(value="/customers/{customerId}/baskets")
    public Iterable<BasketResponse> getAllBasketsByCustomerId(@PathVariable (value = "customerId") int customerId){
        return Streamable.of(basketservice.findByCustomerId(customerId)).map(basket -> {
        	Iterable<Item> items = itemService.findByBasketId(basket.getId());
            return new BasketResponse(basket, items);
    	}).toList();
    }  
    
    @DeleteMapping(value="/customers/{customerId}/baskets")
    public String deleteAllBasketsByCustomerId(@PathVariable("customerId") @Min(1) int id) {
    	basketservice.deleteByCustomerId(id);
        return "All baskets under customer ID :"+id+" is deleted";            
    }
    
    @DeleteMapping(value="/baskets/{id}")
    public String deleteBasket(@PathVariable("id") @Min(1) int id) {
        Basket std = basketservice.findById(id)
                                     .orElseThrow(()->new ResourceNotFoundException("Basket with "+id+" is Not Found!"));
        basketservice.deleteById(std.getId());
        return "Basket with ID :"+id+" is deleted";            
    }
             
    @PostMapping(value="/baskets")
    public Basket addBasket(@Valid @RequestBody BasketRequest basketRequest) {
    	Basket basket = new Basket();
    	basket.setCustomerId(basketRequest.getCustomerId());
    	basket.setName(basketRequest.getName());
    	basket.setItemCount(0);
    	basket.setTotalPrice(0.00);
        return basketservice.save(basket);
    }           
    @PutMapping(value="/baskets/{id}")
    public Basket updateBasket(@PathVariable("id") @Min(1) int id, @Valid @RequestBody Basket newBasket) {
        Basket basket = basketservice.findById(id)
                                     .orElseThrow(()->new ResourceNotFoundException("Basket with "+id+" is Not Found!"));
        basket.setName(newBasket.getName());
        return basketservice.save(basket);   
    }  
}

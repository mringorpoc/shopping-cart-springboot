package com.example.basketservice.controller;

import java.util.Collections;
import java.util.List;

import org.springframework.data.util.Streamable;

import com.example.basketservice.model.Basket;
import com.example.basketservice.model.Item;

public class BasketResponse {

	public BasketResponse() {}
	
	public BasketResponse(Basket basket){
		super();
		this.id = basket.getId();
		this.customerId = basket.getCustomerId();
		this.name = basket.getName();
		this.itemCount = basket.getItemCount();
		this.totalPrice = basket.getTotalPrice();
	}
	
	public BasketResponse(Basket basket, Iterable<Item> items){
		this(basket);
		this.items = Streamable.of(items).toList();
	}
	
	private int id = -1; 

    private int customerId = -1;

	private String name = "";
    
    private int itemCount = -1;
    
    private double totalPrice = -1;
	
    private List<Item> items = Collections.emptyList();
    
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getItemCount() {
		return itemCount;
	}

	public void setItemCount(int itemCount) {
		this.itemCount = itemCount;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	@Override
	public String toString() {
		return "BasketResponse [id=" + id + ", customerId=" + customerId + ", name=" + name + ", itemCount=" + itemCount
				+ ", totalPrice=" + totalPrice + ", items=" + items + "]";
	}
}

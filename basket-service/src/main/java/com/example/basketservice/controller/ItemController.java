package com.example.basketservice.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.basketservice.exception.ResourceNotFoundException;
import com.example.basketservice.model.Item;
import com.example.basketservice.service.ItemService;

@RestController
@RequestMapping("/api")
public class ItemController {

    ItemService itemservice;
	
    @Autowired
    public ItemController(ItemService itemservice) {
        this.itemservice = itemservice;
    }
    
    @GetMapping("/baskets/{basketId}/items")
    public Iterable<Item> getAllItemsByBasketId(@PathVariable (value = "basketId") int basketId) {
        return itemservice.findByBasketId(basketId);
    }
    
    @PostMapping("/baskets/{basketId}/items")
    public Item addItem(@PathVariable (value = "basketId") int basketId, 
    		            @Valid @RequestBody Item item) {
    	
    	Item uItem = itemservice.findByProductIdAndBasketId(item.getProductId(), basketId).orElse(null);    	
    	if (uItem == null) {
    		return itemservice.save(basketId, item);
    	}
    	return itemservice.update(basketId, uItem, item);
    }
    
    @PutMapping("/baskets/{basketId}/items/{itemId}")
    public Item editItem(@PathVariable (value = "basketId") int basketId, 
    		             @PathVariable (value = "itemId") int itemId,
    		             @Valid @RequestBody Item item) {
    	
    	Item uitem = itemservice.findByIdAndBasketId(itemId, basketId)
    			          .orElseThrow(()->new ResourceNotFoundException("Basket id: " + basketId + " does not contain Item with id: "+itemId));

    	return itemservice.update(basketId, uitem, item);
    }
    
    @DeleteMapping("/baskets/{basketId}/items/{itemId}")
    public String deleteItem(@PathVariable (value = "basketId") int basketId, 
    		@PathVariable (value = "itemId") int itemId) {
    	itemservice.deleteById(itemId, basketId);
    	return "Item " + itemId + " was deleted from basket " + basketId; 
    }
    
    @DeleteMapping("/baskets/{basketId}/items")
    public String deleteAllItems(@PathVariable (value = "basketId") int basketId) {
    	itemservice.deleteByBasketId(basketId);
    	return "All items were deleted from basket " + basketId; 
    }
}

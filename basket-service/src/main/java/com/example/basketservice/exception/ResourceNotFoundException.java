package com.example.basketservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException { 
    private static final long serialVersionUID = 1L;
    @SuppressWarnings("unused")
	private String message;
    public ResourceNotFoundException( String message) {
        this.message = message;
    }
}

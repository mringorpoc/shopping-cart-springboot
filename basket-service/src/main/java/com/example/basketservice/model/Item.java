package com.example.basketservice.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Data
@Getter
@Setter
@Accessors(chain=true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "item")
public class Item implements Serializable, Cloneable {

	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private int id; 
    
	@NotNull
	private Long productId;
	
	@NotNull
	@Column(unique = true)
    private String title;
    
	@NotNull
    @Size(max = 10000)
    private String description;
    
    private String image;
    
    private String category;
	
	@NotNull
	private int quantity;
	
	@NotNull
	private double price;
	
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "basket_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonBackReference
	private Basket basket;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}


	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Basket getBasket() {
		return basket;
	}

	public void setBasket(Basket basket) {
		this.basket = basket;
	}

	
	@Override
	public String toString() {
		return "Item [id=" + id + ", productId=" + productId + ", title=" + title + ", description=" + description
				+ ", image=" + image + ", category=" + category + ", quantity=" + quantity + ", price=" + price
				+ ", basket=" + basket + "]";
	}

	@Override
	public Object clone() {
	    try {
	        return (Item) super.clone();
	    } catch (CloneNotSupportedException e) {
	    	
	    	Item newItem = new Item();
	    	newItem.setId(this.id);
	    	newItem.setTitle(this.title);
	    	newItem.setDescription(this.description);
	    	newItem.setImage(this.image);
	    	newItem.setCategory(this.category);
	    	newItem.setPrice(this.price);
	    	newItem.setProductId(this.productId);
	    	newItem.setQuantity(this.quantity);
	    	newItem.setBasket(this.basket);
	        return newItem;
	    }
	}
}

package com.example.basketservice.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.basketservice.model.Basket;

@Repository
public interface BasketRepository extends CrudRepository<Basket, Integer> {
	Optional<Basket> findByName(String name);
	Iterable<Basket> findByCustomerId(int customerId);
}

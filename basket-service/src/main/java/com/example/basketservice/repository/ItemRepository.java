package com.example.basketservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.basketservice.model.Item;

import java.util.Optional;

@Repository
public interface ItemRepository extends JpaRepository<Item, Integer> {
	Iterable<Item> findByBasketId(int basketId);
    Optional<Item> findByIdAndBasketId(int id, int basketId);
    Optional<Item> findByProductIdAndBasketId(Long productId, int basketId);
}

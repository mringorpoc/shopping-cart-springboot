package com.example.basketservice.service;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.basketservice.model.Basket;
import com.example.basketservice.repository.BasketRepository;

@Service
public class BasketService implements IBasketService {
    
	BasketRepository basketRepository;
    
    @Autowired
    public BasketService(BasketRepository basketRepository) {
        this.basketRepository = basketRepository;
    }
    
	@Override
	public Iterable<Basket> findByCustomerId(int customerId) {
		return basketRepository.findByCustomerId(customerId);
	}
	
    @Override
    public Iterable<Basket> getAllBaskets() {
        return basketRepository.findAll();
    }
    @Override
    public Optional<Basket> findById(int id) {
        return basketRepository.findById(id);
    }
    @Override
    public Optional<Basket> findByName(String name) {
        return basketRepository.findByName(name);
    }
    @Override
    public Basket save(Basket basket) {
        return basketRepository.save(basket);
    }
    @Override
    public void deleteById(int id) {
        basketRepository.deleteById(id);
    }

	@Override
	public void deleteByCustomerId(int customerId) {
		basketRepository.findByCustomerId(customerId).forEach(basket -> basketRepository.deleteById(basket.getId()));
	}
}

package com.example.basketservice.service;

import java.util.Optional;

import com.example.basketservice.model.Basket;

public interface IBasketService {
	
    Iterable<Basket> getAllBaskets();
    
    Iterable<Basket> findByCustomerId(int customerId);
    
    Optional<Basket> findById(int id);
    
    Optional<Basket> findByName(String name);
    
    Basket save(Basket basket);
    
    void deleteById(int id);
    
    void deleteByCustomerId(int customerId);
}

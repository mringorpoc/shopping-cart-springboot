package com.example.basketservice.service;

import java.util.Optional;

import com.example.basketservice.model.Item;

public interface IItemService {

	Iterable<Item> findByBasketId(int basketId);
	
	Optional<Item> findByIdAndBasketId(int id, int basketId);
    
	Optional<Item> findByProductIdAndBasketId(Long productId, int basketId);
	
	Item save(int basketId, Item item);
    
	Item update (int basketId, Item existingItem, Item inputItem);
	
	void deleteById(int id, int basketId);
    
	void deleteByBasketId(int basketId);
}

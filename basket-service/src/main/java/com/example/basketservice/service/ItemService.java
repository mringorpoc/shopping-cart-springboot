package com.example.basketservice.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

import com.example.basketservice.exception.ResourceNotFoundException;
import com.example.basketservice.model.Basket;
import com.example.basketservice.model.Item;
import com.example.basketservice.model.remote.Product;
import com.example.basketservice.repository.BasketRepository;
import com.example.basketservice.repository.ItemRepository;
import com.example.basketservice.service.remote.ProductService;

@Service
public class ItemService implements IItemService {

	BasketRepository basketRepository;
	ItemRepository itemRepository;
    ProductService productService;
	
    @Autowired
    public ItemService(BasketRepository basketRepository, ItemRepository itemRepository, ProductService productService) {
        this.basketRepository = basketRepository;
        this.itemRepository = itemRepository;
        this.productService = productService;
    }

	@Override
	public Iterable<Item> findByBasketId(int basketId) {
		return itemRepository.findByBasketId(basketId);
	}

	@Override
	public Optional<Item> findByIdAndBasketId(int id, int basketId) {
		return itemRepository.findByIdAndBasketId(id, basketId);
	}

	@Override
	public Item save(int basketId, final Item item) {

		return basketRepository.findById(basketId).map(basket -> {
			
			Product product = Optional.of(productService.getProductById(item.getProductId()))
					.orElseThrow(()->new ResourceNotFoundException("Product with "+item.getProductId()+" is Not Found!"));
			
			if (item.getQuantity() < 1) { //qty = 0
				return item;
			}

			if (product.getStock() < 1 || product.getStock() < item.getQuantity()) {
					throw new ResourceNotFoundException("Product " + product.getId() 
					+ " " + product.getTitle() + " only has " + product.getStock()+ " is not enough to serve " + item.getQuantity() + ".");
			}
			product.setStock(product.getStock() - item.getQuantity());
			productService.updateProduct(product.getId(), product); //reserve stock
			
			item.setTitle(product.getTitle());
			item.setDescription(product.getDescription());
			item.setCategory(product.getCategory());
			item.setImage(product.getImage());
			item.setProductId(product.getId());
			item.setPrice(product.getPrice());
			item.setQuantity(item.getQuantity());
			
            Iterable<Item> items = itemRepository.findByBasketId(basketId);
			
			int totalCount = Streamable.of(items).filter(e -> e.getProductId() != product.getId())
					             .stream().mapToInt(Item::getQuantity).sum();
			
			double totalPrice = Streamable.of(items).filter(e -> e.getProductId() != product.getId())
		             .stream().mapToDouble(Item::getPrice).sum();

			basket.setItemCount(totalCount + item.getQuantity());
			basket.setTotalPrice(totalPrice + (item.getPrice() * item.getQuantity()));
			
			item.setBasket(basket);
			
			return itemRepository.save(item);
		}).orElseThrow(()->new ResourceNotFoundException("Basket with "+basketId+" is Not Found!"));
	}

	@Override
	public Item update(int basketId, final Item existingItem, Item inputItem) {
		
		return basketRepository.findById(basketId).map(basket -> {

			if (inputItem.getQuantity() < 1) { //qty = 0
				deleteById(inputItem.getId(), basketId);
				return inputItem;
			}
			
			Product product = Optional.of(productService.getProductById(existingItem.getProductId()))
					.orElseThrow(()->new ResourceNotFoundException("Product with "+existingItem.getProductId()+" is Not Found!"));
			
			int stock = inputItem.getQuantity() - existingItem.getQuantity();

			if ( product.getStock() < stock) {
				throw new ResourceNotFoundException("Product " + product.getId() 
						+ " " + product.getTitle() + " only has " + product.getStock()+ " is not enough to serve " + inputItem.getQuantity() + ".");
			}
		    
			product.setStock(product.getStock() - stock);
		    productService.updateProduct(product.getId(), product); //reserve stock
		
		    existingItem.setTitle(product.getTitle());
		    existingItem.setDescription(product.getDescription());
		    existingItem.setCategory(product.getCategory());
		    existingItem.setImage(product.getImage());
		    existingItem.setProductId(product.getId());
		    existingItem.setPrice(product.getPrice());
		    existingItem.setQuantity(inputItem.getQuantity());
			
            Iterable<Item> items = itemRepository.findByBasketId(basketId);
			
			int totalCount = Streamable.of(items).filter(e -> e.getProductId() != product.getId())
					             .stream().mapToInt(Item::getQuantity).sum();
			
			double totalPrice = Streamable.of(items).filter(e -> e.getProductId() != product.getId())
		             .stream().mapToDouble(Item::getPrice).sum();

			basket.setItemCount(totalCount + existingItem.getQuantity());
			basket.setTotalPrice(totalPrice + (product.getPrice() * existingItem.getQuantity()));
			
			existingItem.setBasket(basket);
			
			return itemRepository.save(existingItem);
		}).orElseThrow(()->new ResourceNotFoundException("Basket with "+basketId+" is Not Found!"));
	}

	@Override
	public void deleteById(int id, int basketId) {
		
		Basket basket = basketRepository.findById(basketId).orElseThrow(()->new ResourceNotFoundException("Basket with "+basketId+" is Not Found!"));
		Item item = itemRepository.findById(id).orElseThrow(()->new ResourceNotFoundException("Item with "+id+" is Not Found!"));
		
		Iterable<Item> items = itemRepository.findByBasketId(basketId);

    	basketRepository.save(setBasketSummary(basket, item, items));
    	itemRepository.delete(item);
    	
		Product product = Optional.of(productService.getProductById(item.getProductId()))
				.orElseThrow(()->new ResourceNotFoundException("Product with "+item.getProductId()+" is Not Found!"));
		
		product.setStock(product.getStock() + item.getQuantity());
		productService.updateProduct(product.getId(), product);
	}

	private Basket setBasketSummary(Basket basket, Item item, Iterable<Item> items) {
		int totalCount = Streamable.of(items).stream().mapToInt(Item::getQuantity).sum();
    	double totalPrice = Streamable.of(items).stream().mapToDouble(Item::getPrice).sum();
		
    	final int count = item.getQuantity();
    	final double price = item.getPrice();
    	
    	totalCount = totalCount - count;
    	totalPrice = totalPrice - (price * count);
    	
    	basket.setItemCount(totalCount < 0? 0: totalCount);
    	basket.setTotalPrice(totalPrice < 0? 0: totalPrice);
		return basket;
	}
	
	public void deleteByBasketId(int basketId) {
		itemRepository.findByBasketId(basketId).forEach(item -> {
			deleteById(item.getId(), basketId);
		});
	}

	@Override
	public Optional<Item> findByProductIdAndBasketId(Long productId, int basketId) {
		return itemRepository.findByProductIdAndBasketId(productId, basketId);
	}
}

package com.example.basketservice.service.remote;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.basketservice.model.remote.Product;

@FeignClient(value="PRODUCT-SERVICE", fallback = ProductServiceFallback.class)
public interface ProductService {

    @GetMapping("/api/products")
    @CrossOrigin
    List<Product> getAllProducts();
    
    @GetMapping("/api/products/{id}")
    @CrossOrigin
    Product getProductById(@PathVariable("id") @Min(1) Long id);
    
    @PutMapping("/api/products/{id}")
    @CrossOrigin
    Product updateProduct(@PathVariable("id") @Min(1) Long id, @Valid @RequestBody Product product);
}
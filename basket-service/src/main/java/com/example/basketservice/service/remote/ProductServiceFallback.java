package com.example.basketservice.service.remote;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import com.example.basketservice.model.remote.Product;

public class ProductServiceFallback implements ProductService {

	@Override
	public List<Product> getAllProducts() {
		return new ArrayList<Product>();
	}

	@Override
	public Product getProductById(@Min(1) Long id) {
		return new Product();
	}

	@Override
	public Product updateProduct(@Min(1) Long id, @Valid Product product) {
		return product;
	}

}

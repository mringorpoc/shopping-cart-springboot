package com.example.customerservice.controller;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.customerservice.exception.ResourceNotFoundException;
import com.example.customerservice.model.Customer;
import com.example.customerservice.model.remote.Basket;
import com.example.customerservice.model.remote.BasketRequest;
import com.example.customerservice.model.remote.Item;
import com.example.customerservice.service.CustomerService;
import com.example.customerservice.service.remote.BasketService;

@RestController
@RequestMapping("/api")
public class CustomerController {
    
	CustomerService customerService;
	BasketService basketService;
	
    @Autowired
    public CustomerController(CustomerService customerservice, BasketService basketService) {
        this.customerService = customerservice;
        this.basketService = basketService;
    }
    @GetMapping(value="/customers")
    public Iterable<CustomerResponse> getAllCustomers(){
        return Streamable.of(customerService.getAllCustomers()).map(customer -> {
        	Iterable<Basket> baskets = basketService.getAllBasketsByCustomerId(customer.getId().intValue());
            return new CustomerResponse(customer, baskets);
    	}).toList();
    }
    
    @GetMapping(value="/customers/{id}")
    public CustomerResponse getCustomerById(@PathVariable("id") @Min(1) Long id) {
        Customer customer = customerService.findById(id)
                                    .orElseThrow(()->new ResourceNotFoundException("Customer with "+id+" is Not Found!"));
        Iterable<Basket> baskets = basketService.getAllBasketsByCustomerId(id.intValue());
        return new CustomerResponse(customer, baskets);
    }
    
    @GetMapping(value="/customers/{id}/baskets")
    public Iterable<Basket> getCustomerByIdBaskets(@PathVariable("id") @Min(1) Long id) {
    	customerService.findById(id).orElseThrow(()->new ResourceNotFoundException("Customer with "+id+" is Not Found!"));        
        return basketService.getAllBasketsByCustomerId(id.intValue());
    }
    
    @GetMapping(value="/customers/{id}/baskets/{basketId}")
    public Basket getBasketById (
    		@PathVariable("id") @Min(1) int id, 
    		@PathVariable("basketId") @Min(1) int basketId){
    	return basketService.getBasketById(basketId);
    }
    
    @PostMapping(value="/customers")
    public CustomerResponse addCustomer(@Valid @RequestBody CustomerRequest customerRequest) {
    	Customer customer = new Customer();
    	customer.setName(customerRequest.getName());
        return new CustomerResponse(customerService.save(customer));
    }
        
	@PostMapping(value="/customers/{id}/baskets")
	public Basket addBasket (
			@PathVariable("id") @Min(1) int id, 
			@Valid @RequestBody BasketRequest basketRequest){
		basketRequest.setCustomerId(id);
		return basketService.addBasket(basketRequest);
	}
	
    @PostMapping("/customers/{id}/baskets/{basketId}/items")
    public Item addItem(@PathVariable (value = "id") int id, 
    					@PathVariable (value = "basketId") int basketId, 
    		            @Valid @RequestBody Item item) {
    	return basketService.addItem(basketId, item);
    }
    
    @PutMapping(value="/customers/{id}")
    public CustomerResponse updateCustomer(@PathVariable("id") @Min(1) Long id, @Valid @RequestBody CustomerRequest customerRequest) {
        Customer customer = customerService.findById(id)
                                     .orElseThrow(()->new ResourceNotFoundException("Customer with "+id+" is Not Found!"));
        customer.setName(customerRequest.getName());
        return new CustomerResponse(customerService.save(customer));   
    }  
    
    @PutMapping("/customers/{id}/baskets/{basketId}/items/{itemId}")
    public Item editItem(
    		@PathVariable (value = "id") int id, 
    		@PathVariable (value = "basketId") int basketId, 
            @PathVariable (value = "itemId") int itemId,
            @Valid @RequestBody Item item) {
    	return basketService.editItem(basketId, itemId, item);
    }
    
    @DeleteMapping(value="/customers/{id}")
    public String deleteCustomer(@PathVariable("id") @Min(1) Long id) {
        Customer customer = customerService.findById(id)
                                     .orElseThrow(()->new ResourceNotFoundException("Customer with "+id+" is Not Found!"));
        customerService.deleteById(customer.getId());
        return "Customer with ID :"+id+" is deleted";            
    }
    
	@DeleteMapping(value="/customers/{id}/baskets/{basketId}")
    public String deleteAllBasketsByCustomerId(
    		@PathVariable("id") @Min(1) int id, 
    		@PathVariable("basketId") @Min(1) int basketId) {
		return basketService.deleteBasket(basketId);
	}
	
	@DeleteMapping(value="/customers/{id}/baskets")
    public String deleteBasket(@PathVariable("id") @Min(1) int id) {
		return basketService.deleteAllBasketsByCustomerId(id);
	}
    
    @DeleteMapping("/customers/{id}/baskets/{basketId}/items/{itemId}")
    public String deleteItem(
    		@PathVariable (value = "id") int id, 
    		@PathVariable (value = "basketId") int basketId, 
    		@PathVariable (value = "itemId") int itemId) {
    	return basketService.deleteItem(basketId, itemId);
    }
    
    @DeleteMapping("/customers/{id}/baskets/{basketId}/items")
    public String deleteAllItems(
    		@PathVariable (value = "id") int id,
    		@PathVariable (value = "basketId") int basketId) {
    	return basketService.deleteAllItems(basketId);
    }
}

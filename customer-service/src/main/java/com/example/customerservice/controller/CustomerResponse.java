package com.example.customerservice.controller;

import java.util.Collections;
import java.util.Set;

import org.springframework.data.util.Streamable;

import lombok.Data;

import com.example.customerservice.model.Customer;
import com.example.customerservice.model.remote.Basket;
import com.google.common.base.Optional;

@Data
public class CustomerResponse {

	CustomerResponse(Customer customer){
		this.id = Optional.of(customer.getId()).or(-1l);
		this.name = Optional.of(customer.getName()).or("");
	}
	
	CustomerResponse(Customer customer, Iterable<Basket> baskets){
		this(customer);
		this.baskets = Streamable.of(baskets).toSet();
	}	
	
	private Long id = -1l;

	private String name = "";
	
	private Set<Basket> baskets = Collections.emptySet();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Basket> getBaskets() {
		return baskets;
	}

	public void setBaskets(Set<Basket> baskets) {
		this.baskets = baskets;
	}

	@Override
	public String toString() {
		return "CustomerResponse [id=" + id + ", name=" + name + ", basket=" + baskets + "]";
	}
}

package com.example.customerservice.controller;

public class DeleteResponse {

	private String code;
	
	private String message;
	
	DeleteResponse(String code, String message){
		this.code = code;
		this.message = message;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}

package com.example.customerservice.model.remote;

import java.util.Collections;
import java.util.Set;

import lombok.Data;

@Data
public class Basket extends BasketRequest {

	private String name = "";
    
    private int itemCount = -1;
    
    private double totalPrice = -1;

	private Set <Item> items = Collections.emptySet();
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getItemCount() {
		return itemCount;
	}

	public void setItemCount(int itemCount) {
		this.itemCount = itemCount;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Set<Item> getItems() {
		return items;
	}

	public void setItems(Set<Item> items) {
		this.items = items;
	}

	@Override
	public String toString() {
		return "Basket [name=" + name + ", itemCount=" + itemCount + ", totalPrice=" + totalPrice + ", items=" + items
				+ ", id=" + getId() + ", customerId=" + getCustomerId() + "]";
	}
}

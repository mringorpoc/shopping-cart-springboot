package com.example.customerservice.model.remote;

import lombok.Data;

@Data
public class Item {

	private int id = -1; 

	private Long productId = -1l;

    private String title = "";

    private String description = "";
    
    private String image = "";
    
    private String category = "";

	private double price = -1;
	
	private int quantity = -1;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "Item [id=" + id + ", productId=" + productId + ", title=" + title + ", description=" + description
				+ ", image=" + image + ", category=" + category + ", price=" + price + "]";
	}
}

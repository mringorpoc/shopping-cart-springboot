package com.example.customerservice.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.customerservice.model.Customer;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long>{
	Optional<Customer> findByName(String name);
}

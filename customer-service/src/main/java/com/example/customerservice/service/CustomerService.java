package com.example.customerservice.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.customerservice.model.Customer;
import com.example.customerservice.repository.CustomerRepository;

@Service
public class CustomerService implements ICustomerService{

	CustomerRepository customerRepository;
	
	@Autowired
	CustomerService(CustomerRepository customerRepository){
		this.customerRepository = customerRepository;
	}
	
	@Override
	public Iterable<Customer> getAllCustomers() {
		return customerRepository.findAll();
	}

	@Override
	public Optional<Customer> findById(Long id) {
		return customerRepository.findById(id);
	}

	@Override
	public Optional<Customer> findByName(String name) {
		return customerRepository.findByName(name);
	}

	@Override
	public Customer save(Customer customer) {
		return customerRepository.save(customer);
	}

	@Override
	public void deleteById(Long id) {
		customerRepository.deleteById(id);
	}
}

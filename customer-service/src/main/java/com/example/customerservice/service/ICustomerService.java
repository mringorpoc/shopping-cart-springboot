package com.example.customerservice.service;

import java.util.Optional;

import com.example.customerservice.model.Customer;

public interface ICustomerService {
    
	Iterable<Customer> getAllCustomers();
    
    Optional<Customer> findById(Long id);

    Optional<Customer> findByName(String name);
    
    Customer save(Customer product);
    
    void deleteById(Long id);
}

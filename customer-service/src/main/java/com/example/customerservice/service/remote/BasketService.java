package com.example.customerservice.service.remote;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.customerservice.model.remote.Basket;
import com.example.customerservice.model.remote.BasketRequest;
import com.example.customerservice.model.remote.Item;

@FeignClient("BASKET-SERVICE")
public interface BasketService {

	@GetMapping("/api/customers/{customerId}/baskets")
    @CrossOrigin
	Iterable<Basket> getAllBasketsByCustomerId(@PathVariable (value = "customerId") int customerId);

	@PostMapping(value="/api/baskets")
	@CrossOrigin
	Basket addBasket(@Valid @RequestBody BasketRequest basketRequest);
	
	@DeleteMapping(value="/api/customers/{id}/baskets")
	@CrossOrigin
    String deleteAllBasketsByCustomerId(@PathVariable("id") @Min(1) int id);
	
	@DeleteMapping(value="/api/baskets/{id}")
	@CrossOrigin
    String deleteBasket(@PathVariable("id") @Min(1) int id);
	
    @GetMapping(value="/api/baskets/{id}")
    @CrossOrigin
    Basket getBasketById(@PathVariable("id") @Min(1) int id);
    
    @PostMapping("/api/baskets/{basketId}/items")
    @CrossOrigin
    Item addItem(@PathVariable (value = "basketId") int basketId, 
    		            @Valid @RequestBody Item item);
    
    @PutMapping("/api/baskets/{basketId}/items/{itemId}")
    Item editItem(@PathVariable (value = "basketId") int basketId, 
    		             @PathVariable (value = "itemId") int itemId,
    		             @Valid @RequestBody Item item) ;
    
    @DeleteMapping("/api/baskets/{basketId}/items")
    @CrossOrigin
    String deleteAllItems(@PathVariable (value = "basketId") int basketId);
    
    @DeleteMapping("/api/baskets/{basketId}/items/{itemId}")
    @CrossOrigin
    String deleteItem(@PathVariable (value = "basketId") int basketId, 
    		@PathVariable (value = "itemId") int itemId);
}

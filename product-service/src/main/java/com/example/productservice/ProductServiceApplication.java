package com.example.productservice;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;
import java.util.function.Supplier;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

import com.example.productservice.model.Product;
import com.example.productservice.service.FakeStoreApi;
import com.example.productservice.service.ProductService;

import feign.Feign;
import feign.Logger;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;
import feign.okhttp.OkHttpClient;
import feign.slf4j.Slf4jLogger;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;
import io.vavr.control.Try;

@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class ProductServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductServiceApplication.class, args);
	}
	
	@Autowired
	ProductService productService;
	

	@PostConstruct
    private void init() {//update and refresh items from remote store

		Supplier<List<Product>> decoratedSupplier = CircuitBreaker
			    .decorateSupplier(getFallbackConfig(), getRemoteStoreClient()::getAllProducts);
		
		updateProducts(decoratedSupplier);
    }


	private void updateProducts(Supplier<List<Product>> decoratedSupplier) {
		Try.ofSupplier(decoratedSupplier).recover(throwable -> new ArrayList<Product>())
		   .get().forEach(product -> {
		        productService.findByTitle(product.getTitle()).ifPresentOrElse(productPresent ->{
				copyTo(product, productPresent);
				productPresent.setStock(100);
				productService.save(productPresent);
			}, ()->{
				product.setStock(100);
				productService.save(product);
			});
		});
	}

	private Product copyTo(Product product, Product productPresent) {
		productPresent.setCategory(product.getCategory());
		productPresent.setDescription(product.getDescription());
		productPresent.setImage(product.getImage());
		productPresent.setTitle(product.getTitle());
		productPresent.setPrice(product.getPrice());
		return productPresent;
	}

	private CircuitBreaker getFallbackConfig() {
		CircuitBreakerConfig circuitBreakerConfig = CircuitBreakerConfig.custom()
			    .failureRateThreshold(50)
			    .waitDurationInOpenState(Duration.ofMillis(1000))
			    .permittedNumberOfCallsInHalfOpenState(2)
			    .slidingWindowSize(2)
			    .recordExceptions(IOException.class, TimeoutException.class)
			    .build();

		CircuitBreakerRegistry circuitBreakerRegistry = CircuitBreakerRegistry.of(circuitBreakerConfig);
		CircuitBreaker circuitBreaker = circuitBreakerRegistry.circuitBreaker("fakestoreapi.com");
		return circuitBreaker;
	}

	private FakeStoreApi getRemoteStoreClient() {
		FakeStoreApi fakeStoreApi = Feign.builder()
				  .client(new OkHttpClient())
				  .encoder(new GsonEncoder())
				  .decoder(new GsonDecoder())
				  .logger(new Slf4jLogger(FakeStoreApi.class))
				  .logLevel(Logger.Level.FULL)
				  .target(FakeStoreApi.class, "https://fakestoreapi.com"); //TODO put to env
		return fakeStoreApi;
	}
}

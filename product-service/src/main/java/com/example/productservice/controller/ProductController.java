package com.example.productservice.controller;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.productservice.exception.ResourceNotFoundException;
import com.example.productservice.model.Product;
import com.example.productservice.service.ProductService;

@RestController
@RequestMapping("/api")
public class ProductController {
    
	ProductService productService;
	
    @Autowired
    public ProductController(ProductService productservice) {
        this.productService = productservice;
    }
    @GetMapping(value="/products")
    public Iterable<Product> getAllProducts(){
        return productService.getAllProducts();
    }           
    @GetMapping(value="/products/{id}")
    public Product getProductById(@PathVariable("id") @Min(1) Long id) {
        Product product = productService.findById(id)
                                    .orElseThrow(()->new ResourceNotFoundException("Product with "+id+" is Not Found!"));
        return product;
    }           
    @PostMapping(value="/products")
    public Product addProduct(@Valid @RequestBody Product product) {
        return productService.save(product);
    }           
    @PutMapping(value="/products/{id}")
    public Product updateProduct(@PathVariable("id") @Min(1) Long id, @Valid @RequestBody Product newProduct) {
        Product product = productService.findById(id)
                                     .orElseThrow(()->new ResourceNotFoundException("Product with "+id+" is Not Found!"));
        product.setStock(newProduct.getStock());
        return productService.save(product);   
    }           
    @DeleteMapping(value="/products/{id}")
    public String deleteProduct(@PathVariable("id") @Min(1) Long id) {
        Product std = productService.findById(id)
                                     .orElseThrow(()->new ResourceNotFoundException("Product with "+id+" is Not Found!"));
        productService.deleteById(std.getId());
        return "Product with ID :"+id+" is deleted";            
    }
}

package com.example.productservice.service;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;

import com.example.productservice.model.Product;

import feign.RequestLine;

public interface FakeStoreApi {

	@RequestLine("GET /products")
    @CrossOrigin
    List<Product> getAllProducts();
}

package com.example.productservice.service;

import java.util.Optional;

import com.example.productservice.model.Product;

public interface IProductService {
    
	Iterable<Product> getAllProducts();
    
    Optional<Product> findById(Long id);
    
    Optional<Product> findByTitle(String title);

    Product save(Product product);
    
    void deleteById(Long id);
}

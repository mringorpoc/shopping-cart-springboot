import React from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Header from "./components/Header";
import Main from "./components/Main";

const customerId = 21;

export default function App() {
  const [customer, setCustomer] = React.useState();

  const getCustomer = async () => {
    fetch(`http://localhost:3001/api/customers/${customerId}`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "X-API-KEY": "AKIA547QVSB3LTPPDPMK",
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setCustomer(data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  React.useEffect(() => {
    if (!customer) {
      getCustomer();
    }
  }, [customer]);

  return (
    <React.Fragment>
      <CssBaseline />
      <Header />
      <Main customer={customer} refreshCustomer={getCustomer} />
    </React.Fragment>
  );
}

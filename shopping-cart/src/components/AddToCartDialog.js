import React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

const createBasket = async (customer, name, item, fn, fn2) => {
  if (!customer && !customer.id && !name) return;
  fetch(`http://localhost:3001/api/customers/${customer.id}/baskets`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "X-API-KEY": "AKIA547QVSB3LTPPDPMK",
    },
    body: JSON.stringify({
      customerId: customer.id,
      name: name,
    }),
  })
    .then((res) => res.json())
    .then((data) => {
      fn(customer, data.id, item, fn2);
    })
    .catch((error) => {
      console.log(error);
    });
};

const addToBasket = async (customer, basketId, item, fn) => {
  if (!customer && !customer.id && !basketId && !item) return;
  fetch(
    `http://localhost:3001/api/customers/${customer.id}/baskets/${basketId}/items`,
    {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "X-API-KEY": "AKIA547QVSB3LTPPDPMK",
      },
      body: JSON.stringify({
        id: item.id,
        title: item.title,
        description: item.description,
        category: item.category,
        image: item.image,
        productId: item.id,
        quantity: "1",
        price: item.price,
      }),
    }
  )
    .then((res) => res.json())
    .then((data) => {
      fn(customer);
    })
    .catch((error) => {
      console.log(error);
    });
};

const getBaskets = async (customer, fn) => {
  if (!customer || !customer.id) return;
  fetch(`http://localhost:3001/api/customers/${customer.id}/baskets`, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "X-API-KEY": "AKIA547QVSB3LTPPDPMK",
    },
  })
    .then((res) => res.json())
    .then((data) => {
      const baskets = data;
      fn(baskets);
    })
    .catch((error) => {
      console.log(error);
    });
};

export default function AddToCartDialog(props) {
  const { customer, baskets, refreshCustomer, item } = props;
  const [open, setOpen] = React.useState(false);
  const [name, setName] = React.useState("");
  const [basketId, setBasketId] = React.useState();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const closeDialog = () => {
    setOpen(false);
  };

  const handleNewClose = () => {
    createBasket(customer, name, item, addToBasket, refreshCustomer);
    setOpen(false);
  };

  const handleUpdateClose = () => {
    addToBasket(customer, basketId, item, refreshCustomer);
    setOpen(false);
  };

  const handleOnSelect = (e) => {
    setBasketId(e.target.value);
  };

  const handleOnChange = (e) => {
    setName(e.target.value);
  };

  return (
    <div>
      <Button variant="outlined" color="primary" onClick={handleClickOpen}>
        Add to cart
      </Button>
      <Dialog
        open={open}
        onClose={closeDialog}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title" style={{ width: "500px" }}>
          Add to Cart
        </DialogTitle>

        {(baskets || []).length > 0 ? (
          <>
            <DialogContent>
              <DialogContentText>Select Basket</DialogContentText>
              <Select
                labelId="demo-simple-select-label"
                autoFocus
                margin="dense"
                id="name"
                label="Basket Name"
                fullWidth
                onChange={handleOnSelect}
              >
                {baskets.map((basket) => (
                  <MenuItem value={basket.id}>{basket.name}</MenuItem>
                ))}
              </Select>
            </DialogContent>
            <DialogActions>
              <Button onClick={closeDialog} color="primary">
                Cancel
              </Button>
              <Button onClick={handleUpdateClose} color="primary">
                Submit
              </Button>
            </DialogActions>
          </>
        ) : (
          <>
            <DialogContent>
              <DialogContentText>Select Basket</DialogContentText>
              <TextField
                autoFocus
                margin="dense"
                id="name"
                label="Basket Name"
                type="text"
                fullWidth
                onChange={handleOnChange}
                value={name}
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={closeDialog} color="primary">
                Cancel
              </Button>
              <Button onClick={handleNewClose} color="primary">
                Submit
              </Button>
            </DialogActions>
          </>
        )}
      </Dialog>
    </div>
  );
}

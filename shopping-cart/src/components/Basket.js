import React from "react";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import Typography from "@material-ui/core/Typography";
import useStyles from "./Basket.style";

import BasketCreateDialog from "./BasketCreateDialog";
import BasketDeleteDialog from "./BasketDeleteDialog";
import CartItem from "./CartItem";

const deleteBasket = async (customer, basketId, fn) => {
  if (!customer && !customer.id && !basketId) return;
  fetch(
    `http://localhost:3001/api/customers/${customer.id}/baskets/${basketId}`,
    {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "X-API-KEY": "AKIA547QVSB3LTPPDPMK",
      },
    }
  )
    .then((res) => fn(customer))
    .catch((error) => {
      console.log(error);
    });
};

const getBaskets = async (customer, fn) => {
  if (!customer || !customer.id) return;
  fetch(`http://localhost:3001/api/customers/${customer.id}/baskets`, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "X-API-KEY": "AKIA547QVSB3LTPPDPMK",
    },
  })
    .then((res) => res.json())
    .then((data) => {
      const baskets = data;
      fn(baskets);
    })
    .catch((error) => {
      console.log(error);
    });
};

export default function Basket(props) {
  const classes = useStyles();
  const { customer, refreshCustomer } = props;
  const { baskets } = customer || {};

  const handleBasketDelete = (param) => {
    const { basketId, customer: cust, fn } = param;
    return (e) => {
      deleteBasket(cust, basketId, fn);
    };
  };

  return (
    <div>
      <div className={classes.toolbar}>
        <BasketCreateDialog
          customer={customer}
          refreshBaskets={refreshCustomer}
        />
        <BasketDeleteDialog
          customer={customer}
          refreshBaskets={refreshCustomer}
        />
      </div>
      <Typography variant="h6" className={classes.myCart}>
        {customer ? customer.name : "Guest"}'s Baskets
      </Typography>
      <Divider />
      {(baskets || []).map((basket, index) => {
        return (
          <>
            <List>
              <Typography
                variant="h6"
                style={{ fontSize: "1rem" }}
                className={classes.basketHeading}
              >
                <span>
                  {/*<Avatar>
                    <Button
                      onClick={handleBasketDelete({
                        basketId: basket.id,
                        customer: customer,
                        fn: refreshBaskets,
                      })}
                    >
                      <DeleteIcon />
                    </Button>
                  </Avatar>*/}
                </span>
                <span style={{ marginTop: "8px", marginLeft: "10px" }}>
                  Basket: {basket.name}
                </span>
                <span
                  style={{ marginTop: "8px", right: 5, position: "absolute" }}
                >
                  Total: ${basket.totalPrice.toFixed(2)}
                </span>
              </Typography>
            </List>
            <Divider />
            {!basket.items || basket.items.length === 0 ? (
              <p style={{ marginLeft: "30px" }}>No items in the basket.</p>
            ) : null}
            {basket.items.map((item) => (
              <CartItem
                customer={customer}
                basketId={basket.id}
                key={item.id}
                item={item}
                addToCart={() => {
                  console.log("add into basket");
                }}
                removeFromCart={() => {
                  console.log("remove from cart");
                }}
                refreshBaskets={refreshCustomer}
              />
            ))}
            <Divider />
          </>
        );
      })}

      {/*<List>
        {["Pants - $20", "Pants 1 - $20", "Pants 2 - $20", "Pants 3 - $20"].map(
          (text, index) => (
            <ListItem button key={text}>
              <ListItemIcon>
                {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
              </ListItemIcon>
              <ListItemText primary={text} />
            </ListItem>
          )
        )}
      </List>
      <Divider />
      <List>
        {["All mail", "Trash", "Spam"].map((text, index) => (
          <ListItem button key={text}>
            <ListItemIcon>
              {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
            </ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        ))}
        </List>*/}
    </div>
  );
}

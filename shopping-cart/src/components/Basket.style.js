import { makeStyles } from "@material-ui/core/styles";

const drawerWidth = 420;
const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  drawer: {
    [theme.breakpoints.up("sm")]: {
      width: 0,
      flexShrink: 0,
    },
  },
  myCart: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2, 0, 2),
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  basketHeading: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2, 0, 2),
    display: "flex",
    marginLeft: theme.spacing(2),
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  loginButton: {
    position: "absolute",
    right: theme.spacing(2),
  },
  menuButton: {
    position: "absolute",
    right: theme.spacing(15),
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },
  // necessary for content to be below app bar
  toolbar: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2, 2, 2),
    display: "flex",
  },
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

export default useStyles;

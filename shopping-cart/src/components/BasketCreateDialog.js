import React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

const createBasket = async (customer, name, fn) => {
  if (!customer && !customer.id && !name) return;
  fetch(`http://localhost:3001/api/customers/${customer.id}/baskets`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "X-API-KEY": "AKIA547QVSB3LTPPDPMK",
    },
    body: JSON.stringify({
      customerId: customer.id,
      name: name,
    }),
  })
    .then((res) => res.json())
    .then((data) => {
      fn(customer);
    })
    .catch((error) => {
      console.log(error);
    });
};

export default function BasketCreateDialog(props) {
  const [open, setOpen] = React.useState(false);
  const [name, setName] = React.useState("");
  const { customer, refreshBaskets } = props;

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    createBasket(customer, name, refreshBaskets);
    setOpen(false);
  };

  const handleOnChange = (e) => {
    setName(e.target.value);
  };

  return (
    <div>
      <Button variant="outlined" color="primary" onClick={handleClickOpen}>
        New Basket
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Subscribe</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Please enter a name for your basket.
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Basket Name"
            type="text"
            fullWidth
            onChange={handleOnChange}
            value={name}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleClose} color="primary">
            Submit
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

const deleteAllBaskets = async (customer, fn) => {
  if (!customer && !customer.id) return;
  fetch(`http://localhost:3001/api/customers/${customer.id}/baskets`, {
    method: "DELETE",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "X-API-KEY": "AKIA547QVSB3LTPPDPMK",
    },
  })
    .then((res) => fn(customer))
    .catch((error) => {
      console.log(error);
    });
};

export default function BasketDeleteDialog(props) {
  const [open, setOpen] = React.useState(false);
  const { customer, refreshBaskets } = props;

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    deleteAllBaskets(customer, refreshBaskets);
    setOpen(false);
  };

  return (
    <div>
      <Button
        variant="outlined"
        color="primary"
        onClick={handleClickOpen}
        style={{ marginLeft: "12px" }}
      >
        Delete all Baskets
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Delete all baskets"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Are you sure you want to delete all your baskets?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            No
          </Button>
          <Button onClick={handleClose} color="primary" autoFocus>
            Yes
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

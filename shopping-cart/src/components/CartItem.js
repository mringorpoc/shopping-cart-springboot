import Button from "@material-ui/core/Button";
import { Wrapper } from "./CartItem.styles";

const updateItem = async (customer, basketId, item, qty, fn) => {
  if (!customer && !customer.id && !basketId && !item.id) return;
  fetch(
    `http://localhost:3001/api/customers/${customer.id}/baskets/${basketId}/items/${item.id}`,
    {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "X-API-KEY": "AKIA547QVSB3LTPPDPMK",
      },
      body: JSON.stringify({
        id: item.id,
        title: item.title,
        description: item.description,
        category: item.category,
        image: item.image,
        productId: item.productId,
        quantity: qty,
        price: item.price,
      }),
    }
  )
    .then((res) => fn(customer))
    .catch((error) => {
      console.log(error);
    });
};

const deleteItem = async (customer, basketId, item, fn) => {
  if (!customer && !customer.id && !basketId && !item.id) return;
  fetch(
    `http://localhost:3001/api/customers/${customer.id}/baskets/${basketId}/items/${item.id}`,
    {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "X-API-KEY": "AKIA547QVSB3LTPPDPMK",
      },
    }
  )
    .then((res) => fn(customer))
    .catch((error) => {
      console.log(error);
    });
};

const CartItem = ({
  customer,
  basketId,
  item,
  addToCart,
  removeFromCart,
  refreshBaskets,
}) => (
  <Wrapper>
    <div
      style={{ paddingLeft: "25px", paddingRight: "25px", marginRight: "25px" }}
    >
      <h3>{item.title}</h3>
      <div className="information">
        <p>Price: ${item.price}</p>
        <p>Total: ${(item.quantity * item.price).toFixed(2)}</p>
      </div>
      <div className="buttons">
        <Button
          size="small"
          disableElevation
          variant="contained"
          onClick={() =>
            updateItem(
              customer,
              basketId,
              item,
              item.quantity - 1,
              refreshBaskets
            )
          }
        >
          -
        </Button>
        <p>{item.quantity}</p>
        <Button
          size="small"
          disableElevation
          variant="contained"
          onClick={() =>
            updateItem(
              customer,
              basketId,
              item,
              item.quantity + 1,
              refreshBaskets
            )
          }
        >
          +
        </Button>
      </div>
    </div>
    <img style={{ marginRight: "10px" }} src={item.image} alt={item.title} />
  </Wrapper>
);

export default CartItem;

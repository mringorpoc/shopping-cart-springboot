import React, { useState } from "react";
import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import IconButton from "@material-ui/core/IconButton";
import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Shelf from "./Shelf";
import Footer from "./Footer";
import Basket from "./Basket";
const drawerWidth = 420;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  drawer: {
    [theme.breakpoints.up("sm")]: {
      width: 0,
      flexShrink: 0,
    },
  },
  myCart: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2, 0, 2),
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  loginButton: {
    position: "absolute",
    right: theme.spacing(2),
  },
  menuButton: {
    position: "absolute",
    right: theme.spacing(15),
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },
  // necessary for content to be below app bar
  toolbar: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2, 2, 2),
    display: "flex",
  },
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

function Main(props) {
  const { window, customer, refreshCustomer } = props;
  const classes = useStyles();
  const [mobileOpen, setMobileOpen] = useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const handleNewBasket = (e) => {
    console.log("new basket");
  };

  const container =
    window !== undefined ? () => window().document.body : undefined;

  const handleAddToCart = (item) => {
    console.log("Add to cart");
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            Shopping Cart with Spring
          </Typography>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <AddShoppingCartIcon />
          </IconButton>
          <Button color="inherit" className={classes.loginButton}>
            Login
          </Button>
        </Toolbar>
      </AppBar>
      <main className={classes.content}>
        <Shelf
          baskets={customer.baskets}
          customer={customer}
          handle={handleAddToCart}
          refreshCustomer={refreshCustomer}
        />
        <Footer />
      </main>
      <nav className={classes.drawer} aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden xsDown implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor="right"
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            <Basket
              baskets={customer ? customer.baskets || [] : []}
              customer={customer}
              handleNewBasket={handleNewBasket}
              refreshCustomer={refreshCustomer}
            />
          </Drawer>
        </Hidden>
      </nav>
    </div>
  );
}

Main.propTypes = {
  window: PropTypes.func,
};
export default Main;

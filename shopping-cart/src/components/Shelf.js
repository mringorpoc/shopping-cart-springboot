import React, { useEffect, useState } from "react";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Dotdotdot from "react-dotdotdot";
import AddToCartDialog from "./AddToCartDialog";

const useStyles = makeStyles((theme) => ({
  icon: {
    marginRight: theme.spacing(2),
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  cardGrid: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  cardMedia: {
    paddingTop: "56.25%", // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6),
  },
}));

export default function Shelf(props) {
  const { customer, refreshCustomer, baskets } = props;
  const [products, setProducts] = useState();

  const getProducts = async (fn) => {
    fetch("http://localhost:3001/api/products", {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "X-API-KEY": "AKIA547QVSB3LTPPDPMK",
      },
    })
      .then((res) => res.json())
      .then((data) => {
        fn(data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    if (!products) {
      getProducts(setProducts);
    }
  }, [products]);

  const classes = useStyles();
  return (
    <main>
      <Container className={classes.cardGrid} maxWidth={false}>
        {/* End hero unit */}
        <Grid container spacing={5}>
          {(products || []).map((card, index) => (
            <Grid item key={card + index} xs={12} sm={6} md={4}>
              <Card className={classes.card}>
                <CardMedia
                  className={classes.cardMedia}
                  image={card.image}
                  title={card.title}
                />
                <CardContent className={classes.cardContent}>
                  <Typography gutterBottom variant="h5" component="h2">
                    {card.title}
                  </Typography>
                  <Dotdotdot clamp={3}>
                    <Typography>{card.description}</Typography>
                  </Dotdotdot>
                </CardContent>
                <CardActions>
                  <AddToCartDialog
                    baskets={baskets}
                    item={card}
                    customer={customer}
                    refreshCustomer={refreshCustomer}
                  />
                </CardActions>
              </Card>
            </Grid>
          ))}
        </Grid>
      </Container>
    </main>
  );
}

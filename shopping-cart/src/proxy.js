var express = require("express");
var request = require("request");
var cors = require("cors");
var app = express();

app.use(cors());

app.use("/", function (req, res) {
  var url = "http://3.112.19.128" + req.url;
  console.log(url);
  req.pipe(request({ qs: req.query, uri: url })).pipe(res);
});

app.listen(3001, function () {
  console.log("CORS-enabled web server listening on port 80");
});
